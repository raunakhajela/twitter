-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 09, 2017 at 10:20 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tweety`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
`commentID` int(11) NOT NULL,
  `comment` varchar(140) NOT NULL,
  `commentOn` int(11) NOT NULL,
  `commentBy` int(11) NOT NULL,
  `commentAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `follow`
--

CREATE TABLE IF NOT EXISTS `follow` (
`followID` int(11) NOT NULL,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  `followOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `follow`
--

INSERT INTO `follow` (`followID`, `sender`, `receiver`, `followOn`) VALUES
(1, 1, 8, '0000-00-00 00:00:00'),
(2, 1, 5, '0000-00-00 00:00:00'),
(4, 1, 7, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
`likeID` int(11) NOT NULL,
  `likeBy` int(11) NOT NULL,
  `likeOn` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`likeID`, `likeBy`, `likeOn`) VALUES
(1, 1, 3),
(2, 5, 3),
(3, 5, 6),
(4, 5, 4),
(5, 5, 5),
(6, 1, 4),
(7, 1, 11),
(8, 1, 10),
(9, 1, 9),
(10, 1, 8),
(11, 1, 7),
(12, 1, 5),
(13, 1, 6),
(14, 1, 4),
(15, 1, 3),
(16, 1, 23),
(17, 1, 23),
(18, 1, 26),
(19, 1, 16),
(20, 1, 17),
(21, 1, 31),
(22, 1, 36),
(23, 1, 22),
(24, 8, 5),
(25, 8, 10),
(26, 8, 35),
(27, 8, 67);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
`messageID` int(11) NOT NULL,
  `message` text NOT NULL,
  `messageTo` int(11) NOT NULL,
  `messageFrom` int(11) NOT NULL,
  `messageOn` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`messageID`, `message`, `messageTo`, `messageFrom`, `messageOn`, `status`) VALUES
(7, 'hi', 8, 1, '2017-09-08 10:49:33', 1),
(8, 'Reviews please', 1, 5, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
`ID` int(11) NOT NULL,
  `notificationFor` int(11) NOT NULL,
  `notificationFrom` int(11) NOT NULL,
  `target` int(11) NOT NULL,
  `type` enum('follow','retweet','like','mention') NOT NULL,
  `time` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`ID`, `notificationFor`, `notificationFrom`, `target`, `type`, `time`, `status`) VALUES
(1, 1, 5, 1, 'follow', '2017-09-12 00:00:00', 1),
(2, 8, 1, 51, 'retweet', '2017-09-09 08:27:55', 1),
(3, 1, 8, 5, 'like', '2017-09-09 09:38:26', 1),
(4, 1, 8, 10, 'like', '2017-09-09 09:38:26', 1),
(5, 1, 8, 35, 'like', '2017-09-09 09:38:27', 1),
(6, 1, 8, 38, 'retweet', '2017-09-09 09:38:31', 1),
(7, 1, 8, 67, 'like', '2017-09-09 09:39:42', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trends`
--

CREATE TABLE IF NOT EXISTS `trends` (
`trendID` int(11) NOT NULL,
  `hashtag` varchar(140) NOT NULL,
  `createdOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trends`
--

INSERT INTO `trends` (`trendID`, `hashtag`, `createdOn`) VALUES
(1, 'php', '0000-00-00 00:00:00'),
(2, 'tag', '2017-09-03 17:50:41'),
(3, 'whatastory', '2017-09-08 15:16:37'),
(5, 'farewell', '2017-09-09 13:44:50'),
(6, 'studiolife', '2017-09-09 13:44:50');

-- --------------------------------------------------------

--
-- Table structure for table `tweets`
--

CREATE TABLE IF NOT EXISTS `tweets` (
`tweetID` int(11) NOT NULL,
  `status` varchar(140) NOT NULL,
  `tweetBy` int(11) NOT NULL,
  `retweetID` int(11) NOT NULL,
  `retweetBy` int(11) NOT NULL,
  `tweetImage` varchar(255) NOT NULL,
  `likesCount` int(11) NOT NULL,
  `retweetCount` int(11) NOT NULL,
  `postedOn` datetime NOT NULL,
  `retweetMsg` varchar(140) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tweets`
--

INSERT INTO `tweets` (`tweetID`, `status`, `tweetBy`, `retweetID`, `retweetBy`, `tweetImage`, `likesCount`, `retweetCount`, `postedOn`, `retweetMsg`) VALUES
(5, '', 1, 0, 0, 'users/web security graphic.jpg', 3, 0, '2017-09-02 11:30:33', ''),
(7, '', 1, 0, 0, 'users/ghgffggf.PNG', 1, 0, '2017-09-03 12:31:34', ''),
(8, 'this is my new tweet with #tag', 5, 0, 0, '', 1, 0, '2017-09-03 14:20:41', ''),
(10, '@vicasso whatastory.in', 1, 0, 0, '', 2, 0, '2017-09-03 14:29:18', ''),
(15, 'hello folks', 5, 0, 0, '', 0, 0, '2017-09-04 15:19:17', ''),
(16, 'hello folks', 5, 0, 0, '', 1, 0, '2017-09-04 15:19:38', ''),
(17, 'hello folks', 5, 0, 0, '', 1, 0, '2017-09-04 15:19:47', ''),
(18, '@vicasso http://whatastory.in', 5, 9, 1, '', 1, 0, '2017-09-04 19:35:28', ''),
(19, 'hello folks', 5, 15, 1, '', 0, 0, '2017-09-04 19:35:51', ''),
(20, 'hello folks', 5, 16, 1, '', 0, 0, '2017-09-04 19:37:01', ''),
(22, 'hello folks', 5, 17, 5, '', 1, 0, '2017-09-04 19:39:25', ''),
(23, 'I love whatastory', 1, 3, 5, '', 4, 0, '2017-09-04 19:39:30', ''),
(24, 'Hello tweeety', 5, 13, 1, '', 0, 0, '2017-09-05 17:50:39', ''),
(31, '', 1, 0, 0, 'users/1e2484e1-0807-4f29-8512-0ceb96aedb27.jpg', 1, 0, '2017-09-06 09:45:12', ''),
(33, 'this is my new tweet with #tag', 5, 8, 1, '', 1, 0, '2017-09-03 14:20:41', 'test'),
(34, '#php is cool', 1, 0, 0, '', 0, 0, '2017-09-06 15:13:54', ''),
(35, 'tweety is awesome', 1, 0, 0, '', 1, 0, '2017-09-07 11:15:34', ''),
(37, 'Hey!\r\nSince life is not just pictures I decided to follow the idea of a friend and participate in the game called ''a meeting between friends', 8, 36, 1, '', 1, 0, '2017-09-07 11:16:28', ''),
(38, 'tweety is awesome', 1, 35, 1, '', 0, 0, '2017-09-07 11:15:34', ''),
(39, '#php is cool', 1, 34, 1, '', 0, 0, '2017-09-06 15:13:54', ''),
(40, 'hello folks', 5, 17, 1, '', 1, 0, '2017-09-04 15:19:47', ''),
(41, '#whatastory', 1, 0, 0, '', 0, 0, '2017-09-08 11:46:37', ''),
(42, '#whatastory', 1, 0, 0, 'users/martin_kandra.jpg', 0, 0, '2017-09-08 11:46:55', ''),
(51, 'Anyone need post promotion and backlinks from Reddit? I can post your blog posts through my high karma account in relevant subbredits and yo', 8, 0, 0, '', 0, 0, '2017-09-09 08:24:11', ''),
(64, 'Anyone need post promotion and backlinks from Reddit? I can post your blog posts through my high karma account in relevant subbredits and yo', 8, 51, 1, '', 0, 0, '2017-09-09 08:24:11', ''),
(65, 'Looking for a shoe seller to resale', 1, 0, 0, '', 0, 0, '2017-09-09 09:25:27', ''),
(66, 'tweety is awesome', 1, 38, 8, '', 0, 0, '2017-09-07 11:15:34', ''),
(67, '#php is cool', 1, 0, 0, '', 1, 0, '2017-09-09 09:39:23', ''),
(68, 'Wish you all the best Vinu! Hope you have an awesome journey ahead. #farewell #studiolife', 1, 0, 0, '', 0, 0, '2017-09-09 10:14:49', ''),
(71, 'Offline main log bheekh mangte hain aur online main likes :3', 1, 0, 0, '', 0, 0, '2017-09-09 10:19:17', ''),
(72, 'Build 16 websites from scratch in our 10 week course! ðŸ’»ðŸŽ“', 1, 0, 0, '', 0, 0, '2017-09-09 10:19:37', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(11) NOT NULL,
  `username` varchar(40) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `screenName` varchar(40) NOT NULL,
  `profileImage` varchar(255) NOT NULL,
  `profileCover` varchar(255) NOT NULL,
  `following` int(11) NOT NULL,
  `followers` int(11) NOT NULL,
  `bio` varchar(140) NOT NULL,
  `country` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `email`, `password`, `screenName`, `profileImage`, `profileCover`, `following`, `followers`, `bio`, `country`, `website`) VALUES
(1, 'ayush', 'ayush@whatastory.in', 'd57194846aabf19d7717823f8d7f408b', 'Ayush', 'users/ayush-saxena.jpg', 'users/12.png', 3, 0, '', '', ''),
(5, 'vicasso', 'vicassodestiny@gmail.com', 'bebe68374a49cb41b7c9219e97250044', 'Vikas Tiwari', 'assets/images/defaultprofileImage.png', 'assets/images/defaultCoverImage.png', 0, 1, '', '', ''),
(6, 'shiva', 'shiva@whatastory.in', '69f404925df883e0e5579d65b7768e7c', 'Shiva', 'users/shiva_chauhan.jpg', 'assets/images/defaultCoverImage.png', 0, 0, '', '', ''),
(7, 'suresh', 'suresh@whatastory.in', '0487cc982f7db39c51695026e4bdc692', 'Suresh Kumar', 'users/suresh.jpg', 'assets/images/defaultCoverImage.png', 0, 1, '', '', ''),
(8, 'rohit', 'rohit@whatastory.in', '2d235ace000a3ad85f590e321c89bb99', 'Rohit Dalvi', 'users/rohit_dalvi.jpg', 'assets/images/defaultCoverImage.png', 0, 1, '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
 ADD PRIMARY KEY (`commentID`);

--
-- Indexes for table `follow`
--
ALTER TABLE `follow`
 ADD PRIMARY KEY (`followID`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
 ADD PRIMARY KEY (`likeID`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
 ADD PRIMARY KEY (`messageID`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `trends`
--
ALTER TABLE `trends`
 ADD PRIMARY KEY (`trendID`), ADD UNIQUE KEY `hashtag` (`hashtag`);

--
-- Indexes for table `tweets`
--
ALTER TABLE `tweets`
 ADD PRIMARY KEY (`tweetID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
MODIFY `commentID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `follow`
--
ALTER TABLE `follow`
MODIFY `followID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
MODIFY `likeID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
MODIFY `messageID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `trends`
--
ALTER TABLE `trends`
MODIFY `trendID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tweets`
--
ALTER TABLE `tweets`
MODIFY `tweetID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
