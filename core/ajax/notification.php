<?php
include '../init.php';

if (isset($_POST['showNotification']) && !empty($_POST['showNotification'])) {
  $user_id = $_SESSION['user_id'];
  $data = $getFromM->notificationViewed($user_id);
  echo json_encode(array('notification' => $data->totalN, 'messages' => $data->totalM));
}else{
  header('Location: '.BASE_URL.'index.php');
}
?>
